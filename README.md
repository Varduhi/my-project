```mermaid
graph TD;
  Support-->Idea;
  Chat-->Idea;
  Idea-->Code;
  Support-->Learning;
  Code-->Test;
  Code-->Review;
  Code-->Build;
  Build-->Delivery;
  Review-->Documentation;
  Test-->Security;
  Test-->Quality;
  Delivery-->Deploy;
  Documentation-->Deploy;
  Security-->Deploy;
  Quality-->Deploy;
  Deploy-->Operate;
  
```

******************************************************************


```mermaid
graph TD;

  Idea-->GitLab;
  Idea-->Jira;

  ```
  ```mermaid
  graph TD;
  Chat-->Mattermost;
  Chat-->Jira;
```
```mermaid
graph TD;
  Support-->Jira;
  Jira-->Learning;
  Jira-->Code;
  Code-->GitLab;
  Learning-->SDCO;
```
  ```mermaid
  graph TD;
  Build-->GitLab;
  Build-->Kazan;
```
  ```mermaid
  graph TD;
  Delivery-->JFrog;
  Delivery-->Nexus;
```
  ```mermaid
  graph TD;
  Review-->Gitlab;

  ```

  ```mermaid
  graph TD;
  Documentation-->Gitlab;
  Documentation-->Dokuwiki;
  Test-->Jenkins;
  Test-->STF;
  Quality-->Sonar;
  Security-->Sonar;
  Deploy-->ACACIA;
  Operate-->Box;
```